let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
       return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
  collection[collection.length] = element;
  return collection;

}

function dequeue(){
    let removeCollection = [];
  if (collection.length !== 0) {
    for (let i = 1; i < collection.length; i++) {
      removeCollection[i - 1] = collection[i];
    }
  }
  collection = removeCollection;

  return collection;
}

 


function front() {
    
  if (collection.length === 0) return null;
  return collection[0];

}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   

  let count = 0;
  let index = 0;
  while (collection[index] !== undefined) {
    count++;
    index++;
  }
  return count;
}


function isEmpty() {
    //it will check whether the function is empty or not

  let index = 0;
  while (collection[index] !== undefined) {
    index++;
  }
  return index === 0;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};